package com.example.decerto.web;

import com.example.decerto.DecertoApplicationTests;
import com.example.decerto.entity.Quote;
import com.example.decerto.repository.QuoteRepository;
import com.example.decerto.service.QuoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class QuoteControllerTest extends DecertoApplicationTests {
    public static final String NEW_QUOTE = "new quote";
    public static final String NEW_AUTHOR = "new author";
    public static final String PUBLIC_API_QUOTES = "/public/api/quotes";
    public static final String UPDATED_AUTHOR = "updated author";
    public static final String UPDATED_TEXT = "updated text";

    @Autowired
    private QuoteRepository quoteRepository;

    @Autowired
    private QuoteService quoteService;

    @Autowired
    private MockMvc mockMvc;
    private ObjectWriter objectWriter;

    @BeforeAll
    public void beforeAll() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        objectWriter = mapper.writer().withDefaultPrettyPrinter();
    }

    @BeforeEach
    public void beforeEach() {
        prepareListOfQuotes();
    }

    @AfterEach
    @Transactional
    public void afterEach() {
        quoteRepository.deleteAll();
        quoteService.truncate();
    }

    @Test
    public void shouldBeOkayForFiveElements() throws Exception {
        this.mockMvc.perform(get(PUBLIC_API_QUOTES)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value("5"));
    }

    @Test
    public void shouldBeOkayWhileAddingElement() throws Exception {
        Quote quoteToSave = new Quote();
        quoteToSave.setAuthor(NEW_AUTHOR);
        quoteToSave.setText(NEW_QUOTE);

        String content = objectWriter.writeValueAsString(quoteToSave);

        this.mockMvc.perform(post(PUBLIC_API_QUOTES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("6"))
                .andExpect(jsonPath("$.text").value(NEW_QUOTE))
                .andExpect(jsonPath("$.author").value(NEW_AUTHOR));

        assertThat(quoteRepository.count()).isEqualTo(6);
    }

    @Test
    public void shouldBeNoContentAfterDeletingElement() throws Exception {
        this.mockMvc.perform(delete(PUBLIC_API_QUOTES + "/5"))
                .andExpect(status().isNoContent());

        List<Quote> quotes = quoteRepository.findAll();

        quotes.forEach(quote -> {
            assertThat(quoteRepository.count()).isNotEqualTo(5);
        });
    }

    @Test
    public void shouldThrowExceptionAfterWrongIdDeletion() throws Exception {
        this.mockMvc.perform(delete(PUBLIC_API_QUOTES + "/5000"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Cannot find object with that id: [5000]"));
    }

    @Test
    public void shouldBeOkayAfterUpdate() throws Exception {
        Quote quote = quoteService.get(3L);

        assertThat(quote.getId()).isEqualTo(3L);
        assertThat(quote.getText()).isEqualTo("Quote3");
        assertThat(quote.getAuthor()).isEqualTo("Author3");

        quote.setAuthor(UPDATED_AUTHOR);
        quote.setText(UPDATED_TEXT);

        String content =  objectWriter.writeValueAsString(quote);

        this.mockMvc.perform(put(PUBLIC_API_QUOTES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.author").value(UPDATED_AUTHOR))
                .andExpect(jsonPath("$.text").value(UPDATED_TEXT));
    }


    @Test
    public void shouldBeNotOkayAfterCallingUpdate() throws Exception {
        Quote quote = new Quote();
        quote.setText(UPDATED_TEXT);
        quote.setAuthor(UPDATED_AUTHOR);

        String content = objectWriter.writeValueAsString(quote);

        this.mockMvc.perform(put(PUBLIC_API_QUOTES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isBadRequest())
                .andExpect((jsonPath("$").value("Cannot update object without ID")));
    }

    private void prepareListOfQuotes() {
        List<Quote> quotes = new ArrayList<>();

        LongStream.rangeClosed(0, 5).forEach(counter -> {
            quotes.add(new Quote(counter, "Author" + counter, "Quote" + counter));
        });

        quoteRepository.saveAll(quotes);
    }
}
