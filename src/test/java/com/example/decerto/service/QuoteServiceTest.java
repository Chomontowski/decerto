package com.example.decerto.service;

import com.example.decerto.entity.Quote;
import com.example.decerto.exception.ResourceNotFoundException;
import com.example.decerto.repository.QuoteRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class QuoteServiceTest {

    public static final String SAVED_AUTHOR = "saved author";
    public static final String SAVED_TEXT = "saved text";
    public static final String AUTHOR = "author";
    public static final String TEXT = "text";
    @Mock
    private QuoteRepository quoteRepository;

    @InjectMocks
    private QuoteService quoteService;
    public static final Quote GOOD_ID_QUOTE = new Quote(1L, AUTHOR, TEXT);


    @BeforeAll
    public void beforeAll() {
        Quote quoteAfterSave = new Quote(1000L, SAVED_AUTHOR, SAVED_TEXT);

        when(quoteRepository.findAll(any(Pageable.class))).thenReturn(preparePage());
        when(quoteRepository.save(any(Quote.class))).thenReturn(quoteAfterSave);
        when(quoteRepository.findById(1L)).thenReturn(Optional.of(GOOD_ID_QUOTE));
        when(quoteRepository.findById(1000L)).thenReturn(Optional.empty());
        doNothing().when(quoteRepository).delete(GOOD_ID_QUOTE);
    }

    @Test
    public void shouldReturnPageWithFiveElements() {
        Page<Quote> quotesPage = quoteService.getAll(PageRequest.of(0, 20));

        assertThat(quotesPage.getTotalElements()).isEqualTo(6);
    }

    @Test
    public void shouldReturnNewQuoteAfterSave() {
        Quote quote = quoteService.save(new Quote());

        assertThat(quote.getId()).isEqualTo(1000L);
        assertThat(quote.getAuthor()).isEqualTo(SAVED_AUTHOR);
        assertThat(quote.getText()).isEqualTo(SAVED_TEXT);
    }

    @Test
    public void shouldDoNothingWhenDeleting() {
        quoteService.delete(1L);
    }

    @Test
    public void shouldThrowExceptionWhenDeletingBadIdQuote() {
        assertThrows(ResourceNotFoundException.class, () -> {
            quoteService.delete(1000L);
        });
    }

    @Test
    public void shouldThrowExceptionWhileTryingToUpdateNonIdQuote() {
        assertThrows(RuntimeException.class, () -> {
            quoteService.update(new Quote(null, " ", ""));
        });
    }

    private Page<Quote> preparePage() {
        return new PageImpl<>(prepareListOfQuotes(), PageRequest.of(0, 20), 5);
    }

    private List<Quote> prepareListOfQuotes() {
        List<Quote> quotes = new ArrayList<>();

        LongStream.rangeClosed(0, 5).forEach(counter -> {
            quotes.add(new Quote(counter, "Author" + counter, "Quote" + counter));
        });

        return quotes;
    }
}
