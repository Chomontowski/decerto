package com.example.decerto.repository;

import com.example.decerto.entity.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface QuoteRepository extends JpaRepository<Quote, Long> {

    @Query(value = "truncate table quote RESTART IDENTITY", nativeQuery = true)
    @Modifying
    void truncate();
}
