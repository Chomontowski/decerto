package com.example.decerto.service;

import com.example.decerto.entity.Quote;
import com.example.decerto.exception.ResourceNotFoundException;
import com.example.decerto.repository.QuoteRepository;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Data
@Service
public class QuoteService {

    private final QuoteRepository quoteRepository;

    public Page<Quote> getAll(Pageable page) {
        return quoteRepository.findAll(page);
    }

    public Quote save(Quote quote) {
        return quoteRepository.save(quote);
    }

    public void delete(Long id) {
        quoteRepository.delete(get(id));
    }

    public Quote update(Quote quote) {
        if(quote.getId() == null) {
            throw new RuntimeException("Cannot update object without ID");
        }

        return quoteRepository.save(quote);
    }

    public Quote get(Long id) {
        return quoteRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Cannot find object with that id: [" + id + "]")
        );
    }

    @Transactional
    public void truncate() {
        quoteRepository.truncate();
    }
}
