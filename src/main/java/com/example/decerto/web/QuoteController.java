package com.example.decerto.web;

import com.example.decerto.entity.Quote;
import com.example.decerto.service.QuoteService;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Data
@RestController
@RequestMapping("/public/api/quotes")
public class QuoteController {

    private final QuoteService quoteService;

    @GetMapping
    public Page<Quote> getAll(@RequestParam(value = "size", defaultValue = "30") int size,
                              @RequestParam(value = "page", defaultValue = "1") int page) {
        return quoteService.getAll(PageRequest.of(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Quote> get(@PathVariable Long id) {
        return ResponseEntity.ok(quoteService.get(id));
    }

    @PostMapping
    public ResponseEntity<Quote> add(@RequestBody Quote quote) {
        return ResponseEntity.ok(quoteService.save(quote));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        quoteService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public ResponseEntity<Quote> update(@RequestBody Quote quote) {
        return ResponseEntity.ok(quoteService.update(quote));
    }
}
